package sirimeen.hig.assignment1;

import android.content.Intent;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



public class MapsActivity extends FragmentActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Location location;
    public final static String POSITION_MESSAGE = "sirimeen.hig.assignment1.position";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();

    //Recieves the intent and the data. Uses the the methods to retrieve longitude and latitude from location object
    // and uses the coordinates to place a marker on the map
        Intent intent = getIntent();
        location = intent.getParcelableExtra(MainActivity.LOCATION_EXTRA);
        setUpMap(location.getLatitude(), location.getLongitude());
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();

        }
    }

    /**
     * This method takes two parameters and places a marker on the map. It also zoom to the location
     * so you don't have to zoom in yourself
     *
     * @param latitude Here you place the latitude coordinates
     *
     * @param longitude Here you place the longitude coordinates
     */
    private void setUpMap(double latitude, double longitude) {
        LatLng position = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(position).title("Marker"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
    }


    /**
     * This onClick method gets the coordinates and send the data with an intent to the next activity
     * @param view The reference to the view that called the method
     */
    public void getLocationCoordinates(View view) {

        Intent intent = new Intent(this, LocationActivity.class);
        intent.putExtra(POSITION_MESSAGE, location);
        startActivity(intent);
    }


}
