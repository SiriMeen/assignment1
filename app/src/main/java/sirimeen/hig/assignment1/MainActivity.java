package sirimeen.hig.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/*
    Tutorials used for this project
    Location and getting GPS coordinates : Google Api Client http://blog.teamtreehouse.com/beginners-guide-location-android
    Intent & Activity: http://developer.android.com/training/basics/firstapp/starting-activity.html
    SQLite database: https://www.thenewboston.com/videos.php?cat=278&video=27385
    ArrayAdapter : http://stackoverflow.com/questions/2265661/how-to-use-arrayadaptermyclass

 */

public class MainActivity extends Activity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient clientApi;
    private Location lastPosition;
    public final static String LOCATION_EXTRA = "sirimeen.hig.assignment1.location";


    //Builds a Google Api Client
    protected synchronized void buildGoogleApiClient(){
        clientApi = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buildGoogleApiClient();


    }

    //Connects the Api Client
    @Override
    protected void onResume(){
        super.onResume();
        clientApi.connect();
    }

    //Gets the last know location. Most cases it's the users current location
    @Override
    public void onConnected(Bundle bundle){
        lastPosition = LocationServices.FusedLocationApi.getLastLocation(clientApi);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    //onClick method. Gets the last know location and send the location data to the next activity
    public void getLastKnowLocation(View view) {
        if(lastPosition != null) {
            Intent intent = new Intent(this, MapsActivity.class);

            intent.putExtra(LOCATION_EXTRA, lastPosition);
            startActivity(intent);
        }else
            Toast.makeText(this, "You must turn on your GPS", Toast.LENGTH_LONG).show();

    }
    //onClick method. Sends the user to the list that contains previous saved locations
    public void getLocationData(View view) {

        Intent intent = new Intent(this,LocationList.class);
        startActivity(intent);
    }
}
