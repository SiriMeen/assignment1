package sirimeen.hig.assignment1;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

/**
 * The tutorial used to make the SQLite database
 * This video was use to create DBManager class and LocationData class
 * https://www.thenewboston.com/videos.php?cat=278&video=27385
 *
 */

public class DBManager extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "locations.db";
    public static final String TABLE_LOCATIONS = "positions";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_LAT = "latitude";
    public static final String COLUMN_LON = "longitude";
    public static final String COLUMN_ALT = "altitude";
    public static final String COLUMN_TEMP = "temperature";
    public static final String COLUMN_TIMESTAMP = "time";

    /**
     * The constructor. This method creates a object to help create, open or manage a database
     *
     * @param context used to open or create the database
     */

    public DBManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * Creates the tables in the database
     * @param db The database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE " + TABLE_LOCATIONS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_LAT + " REAL, " +
                COLUMN_LON + " REAL, " +
                COLUMN_ALT + " REAL, " +
                COLUMN_TEMP + " REAL, " +
                COLUMN_TIMESTAMP + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP " +
                ");";
        db.execSQL(query);

    }

    /**
     * You call this methood when the database needs to upgrade
     *
     * @param db The database
     * @param oldVersion The old database version
     * @param newVersion The new database version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATIONS);
        onCreate(db);
    }

    /**
     * This method add the data into the database
     *
     * @param location Here you put the the location object
     */
    public void addLocation(LocationData location){

        ContentValues values = new ContentValues();
        values.put(COLUMN_LAT, location.getLat());
        values.put(COLUMN_LON, location.getLon());
        values.put(COLUMN_ALT, location.getAlt());
        //values.put(COLUMN_TEMP, location.getTemp());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_LOCATIONS, null, values);
        db.close();


    }

    //Select all the data and returns the result
   public List<LocationData> getAllData(){
       List<LocationData> locationList = new ArrayList<LocationData>();
       String query = "SELECT * FROM " + TABLE_LOCATIONS;
       SQLiteDatabase db = this.getWritableDatabase();
       Cursor c = db.rawQuery(query, null);

       if(c.moveToFirst()){
           do {
               LocationData data = new LocationData(
                       Double.parseDouble(c.getString(1)),
                       Double.parseDouble(c.getString(2)),
                       Double.parseDouble(c.getString(3)));
               locationList.add(data);
           }while(c.moveToNext());

       }//else Toast.makeText(this, "You don't hav any location records", Toast.LENGTH_LONG).show();
        c.close();
       return locationList;
   }

}
