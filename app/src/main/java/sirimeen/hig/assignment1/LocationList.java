package sirimeen.hig.assignment1;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;


/**
 * Tutorial used her is modified from
 * http://stackoverflow.com/questions/2265661/how-to-use-arrayadaptermyclass
 * used for making the LocationDataAdapter class
 */

public class LocationList extends Activity {

    DBManager dbHandler;
    ListView locationList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_list);


        showData();
    }

    /**
     * Show the data from the database
     */

    public void showData(){
        dbHandler = new DBManager(this);

        List <LocationData> data = dbHandler.getAllData();

        ListAdapter adapterList = new LocationDataAdapter(this, data);
        locationList = (ListView) findViewById(R.id.locationListView);

        locationList.setAdapter(adapterList);

        dbHandler.close();

    }

    /**
     * the adapter class for the listview and database
     *
     */

    private class LocationDataAdapter extends ArrayAdapter<LocationData>{

        LocationDataAdapter(Context context, List <LocationData> locationDataList){

            super(context, R.layout.list_item_layout, locationDataList);

        }


        @Override
        public View getView(int pos, View view, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            View inflaterView = inflater.inflate(R.layout.list_item_layout, parent, false);
            String stringItem =  "Latitude: " + getItem(pos).getLat() + "\n" +
                                "Longitude: " + getItem(pos).getLon();

            TextView textView = (TextView) inflaterView.findViewById(R.id.row_item);
            textView.setText(stringItem);

            return inflaterView;
        }

    }

}
