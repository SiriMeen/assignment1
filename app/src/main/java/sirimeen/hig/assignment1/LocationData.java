package sirimeen.hig.assignment1;


public class LocationData {

    private int id;
    private double lat;
    private double lon;
    private double alt;
    private double temp;
    private String timestamp;

    public LocationData(){

    }

    /**
     * The class constructor
     *
     * @param lat Here goes the latitude coordinates
     * @param lon Here goes the latitude coordinates
     * @param alt Here goes the latitude coordinates
     */

    public LocationData(double lat, double lon, double alt) {
        this.lat = lat;
        this.lon = lon;
        this.alt = alt;
       // this.temp = temp;
    }
    /*
    Setters
     */

    public void setId(int id) {
        this.id = id;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setAlt(double alt) {
        this.alt = alt;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    /*
    Getters
     */

    public int getId() {
        return id;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public double getAlt() {
        return alt;
    }

    public double getTemp() {
        return temp;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
