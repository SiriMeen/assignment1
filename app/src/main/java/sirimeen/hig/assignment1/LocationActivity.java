package sirimeen.hig.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class LocationActivity extends Activity {

    private Location location;
    TextView position;

    DBManager dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        //Recieves the intent and prints out the latitude, longitude and altitude from the location object
        Intent intent = getIntent();
        location = intent.getParcelableExtra(MapsActivity.POSITION_MESSAGE);
        position = (TextView) findViewById(R.id.location_text_view);
        position.setText("Latitude " + location.getLatitude() + "\n" + "Longitude " + location.getLongitude() + "\n" + "Altitude " + location.getAltitude());

        dbHandler = new DBManager(this);
    }

    /**
     * This onClick method saves the coordinates to the SQLite database
     *
     * @param view The reference to the view that called the method
     */
    public void saveLocationData(View view) {
        LocationData locationData = new LocationData(location.getLatitude(), location.getLongitude(), location.getAltitude());
        dbHandler.addLocation(locationData);
    }
}
